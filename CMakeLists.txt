cmake_minimum_required (VERSION 3.0.2)
project (teddyAndBodacious)

include_directories(
  tls-http2-server
  ${PROJECT_BINARY_DIR}
)
configure_file(test.h.in test.h)

add_executable(
	teddyAndBodacious
	test.c
)
add_subdirectory(tls-http2-server)
target_link_libraries(
	teddyAndBodacious
	tls-http2-server
  )
