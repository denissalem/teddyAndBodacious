#    Copyright 2018 Denis Salem
#
#    This file is part of TeddyAndBodacious.
#
#    TeddyAndBodacious is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    TeddyAndBodacious is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with TeddyAndBodacious.  If not, see <http://www.gnu.org/licenses/>.

cmake_minimum_required (VERSION 3.0)
project (TeddyAndBodacious)
include(ExternalProject)

set(EXTERNAL_INSTALL_LOCATION ${CMAKE_BINARY_DIR}/external)

ExternalProject_Add(json
    GIT_REPOSITORY https://github.com/nlohmann/json
    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${EXTERNAL_INSTALL_LOCATION}
)

include_directories(
  ${PROJECT_BINARY_DIR} 
  ${PROJECT_SOURCE_DIR} 
  ${EXTERNAL_INSTALL_LOCATION}
)

configure_file(json_file_handler.hpp.in json_file_handler.hpp)
add_library(JSONFileHandler json_file_handler.cpp)
add_dependencies(JSONFileHandler json)

add_subdirectory("unittest")

