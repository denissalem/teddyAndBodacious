/*
Copyright 2018 Denis Salem

This file is part of TeddyAndBodacious.

TeddyAndBodacious is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TeddyAndBodacious is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TeddyAndBodacious.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "json_file_handler.hpp"

JSONFileHandler::JSONFileHandler() {}
JSONFileHandler::~JSONFileHandler() {}

JSONFileHandler::JSONFileHandler(std::string filename)
{
	std::ifstream json_file(filename);
	assert(json_file.is_open());
	std::string jsonFileContent;
	std::string line;
	while( std::getline(json_file, line) )
	{
		jsonFileContent += line + "\n";
	}
	this->data = json::parse(jsonFileContent);
	json_file.close();
}

void JSONFileHandler::writeFile(std::string filename)
{
    std::ofstream out(filename);
    out << this->data.dump();
    out.close();
}

BranchFileHandler JSONFileHandler::getBranch(std::vector<std::string> branch) {
	json * node = &(this->data);
	for (int i=0; i < branch.size(); i++) {
		if (node->find(branch[i]) == node->end()) {
			throw branch[i];
		}
		node = &((*node)[branch[i]]);
	}
	return BranchFileHandler(this, node);
}

BranchFileHandler::BranchFileHandler(JSONFileHandler * json_data, json * branch) 
{
	this->json_data = json_data;
	this->data = branch;
}

void BranchFileHandler::writeFile(std::string filename)
{
	this->json_data->writeFile(filename);
}
