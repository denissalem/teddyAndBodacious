/*
Copyright 2018 Denis Salem

This file is part of TeddyAndBodacious.

TeddyAndBodacious is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TeddyAndBodacious is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TeddyAndBodacious.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "json_related.hpp"

void json_related()
{
	std::string inputString;
	std::string inputStringVariante;
	
	std::string outputString = "{\"OS\":{\"Free\":[\"Linux\",\"ReactOS\",\"Haiku\"],\"Non-Free\":[\"Windows\",\"MacOS\"]}}";
	JSONFileHandler jsonFileHandlerWriteString; 
	jsonFileHandlerWriteString.data = json::parse(outputString);
	jsonFileHandlerWriteString.writeFile("test.json");
	
	
	// EQUALITY BETWEEN INPUT AND OUTPUT
	JSONFileHandler jsonFileHandlerReadString("test.json");
	inputString += jsonFileHandlerReadString.data.dump();
	assert(inputString == outputString);
	
	// EQUALITY BETWEEN TWO DUMPS (SPACE, SEMI-COLON, END OF LINE)
	std::string stringVariante = "{\"OS\"\t:\n{\"Free\" : [\"Linux\",\"ReactOS\",\"Haiku\"],\"Non-Free\":[\"Windows\",\"MacOS\"]}}";
	JSONFileHandler jsonFileHandlerStringVariante; 
	jsonFileHandlerStringVariante.data = json::parse(stringVariante);
	inputStringVariante += jsonFileHandlerStringVariante.data.dump();
	assert(inputStringVariante == inputString);
	
	// OBJECT INEQUALITY
	std::string stringSomethingElse = "{\"Gender\"\t:\n[\"Male\",\"Female\",\"Non binary\",\"Gender fluid\",\"Agender\"]}";
	JSONFileHandler jsonFileHandlerStringSomethingElse; 
	jsonFileHandlerStringSomethingElse.data = json::parse(stringSomethingElse);
	
	// OBJECT EQUALITY
	assert(jsonFileHandlerReadString.data == jsonFileHandlerStringVariante.data);
	
	// OBJECT INEQUALITY ON TWO DIFFERENT INPUT STRINGS
	assert(jsonFileHandlerReadString.data != jsonFileHandlerStringSomethingElse.data);

	// MODIFY JSON
	std::string stringStallmanFix = "{\"OS\":{\"Free\":[\"GNU/Linux\",\"ReactOS\",\"Haiku\"],\"Non-Free\":[\"Windows\",\"MacOS\"]}}";
	jsonFileHandlerWriteString.data["OS"]["Free"][0] = "GNU/Linux";
	jsonFileHandlerWriteString.writeFile("test.json");
	jsonFileHandlerReadString = JSONFileHandler("test.json");
	assert(jsonFileHandlerReadString.data == jsonFileHandlerWriteString.data);

	// ACCESS BRANCH
	BranchFileHandler branchFileHandler = jsonFileHandlerWriteString.getBranch({"OS"});
	assert((*branchFileHandler.data)["Free"][0] == "GNU/Linux");

	// MODIFY, WRITE & READ BRANCH
	(*branchFileHandler.data)["Free"].push_back("LineageOS");
	branchFileHandler.writeFile("test.json");
	jsonFileHandlerReadString = JSONFileHandler("test.json");
	assert(jsonFileHandlerReadString.data["OS"]["Free"][3] == "LineageOS");

	// PREVENT WRONG ACCESS
	bool passBranchWrongAccessTest = true;
	try
	{ 	
		branchFileHandler = jsonFileHandlerWriteString.getBranch({"Food"});
		passBranchWrongAccessTest = false;
	}
	catch(std::string node) {
	}
	assert(passBranchWrongAccessTest);

	// PREVENT NULL DEPENDENCY USAGE
	jsonFileHandlerWriteString = JSONFileHandler(); 

}

