cmake_minimum_required (VERSION 3.0.2)
project (tls-http2-server)

add_library(
	tls-http2-server
	app_context.c
	events.c
	http2.c
	tls.c
)

find_library(LIB_CRYPTO NAMES crypto)
if(NOT LIB_CRYPTO)
	message(FATAL_ERROR "crypto not found")
endif()

find_library(LIB_SSL NAMES ssl)
if(NOT LIB_SSL)
	message(FATAL_ERROR "ssl not found")
endif()

find_library(LIB_EVENT NAMES event)
if(NOT LIB_EVENT)
	message(FATAL_ERROR "event not found")
endif()

find_library(LIB_EVENT_OPENSSL NAMES event_openssl)
if(NOT LIB_EVENT_OPENSSL)
	message(FATAL_ERROR "event_openssl not found")
endif()

find_library(LIB_NGHTTP2 NAMES nghttp2)
if(NOT LIB_NGHTTP2)
	message(FATAL_ERROR "nghttp2 not found")
endif()


target_link_libraries(
	tls-http2-server
	${LIB_CRYPTO}
	${LIB_EVENT}
	${LIB_EVENT_OPENSSL}
	${LIB_NGHTTP2}
	${LIB_SSL}
)
