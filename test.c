#include "test.h"

static void run(const char *service, const char *key_file, const char *cert_file) {
	SSL_CTX *ssl_ctx;
	app_context app_ctx;
	struct event_base *evbase;

	ssl_ctx = create_ssl_ctx(key_file, cert_file);
	evbase = event_base_new();
	initialize_app_context(&app_ctx, ssl_ctx, evbase);
	server_listen(evbase, service, &app_ctx);

	event_base_loop(evbase, 0);

	event_base_free(evbase);
	SSL_CTX_free(ssl_ctx);
}

int main(int argc, char **argv) {
	struct sigaction act;

	if (argc < 4) {
		fprintf(stderr, "Usage: libevent-server PORT KEY_FILE CERT_FILE\n");
		exit(EXIT_FAILURE);
	}

	memset(&act, 0, sizeof(struct sigaction));
	act.sa_handler = SIG_IGN;
	sigaction(SIGPIPE, &act, NULL);

	SSL_load_error_strings();
	SSL_library_init();

	run(argv[1], argv[2], argv[3]);
	return 0;
}

